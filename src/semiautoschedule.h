#ifndef SEMIAUTOSCHEDULE_H
#define SEMIAUTOSCHEDULE_H
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <Halide.h>
#include <vector>
#include <functional>
#include "halide_image_io.h"
#include "halide_benchmark.h"
#include "FunctionDAG.h"
#include "AutoSchedule.h"
struct AutoSchduleFunction{
    std::string scope;
};
struct FScope{
    std::vector<int> optimal_regsiter_tiling;
    std::vector<int> max_regsiter_tiling;
    int threads;
};
struct SemiScheduler{
    map<std::string,std::function<void()>> customschedule;
    map<std::string,AutoSchduleFunction> autoschedule;
    map<std::string,FScope> scopes;
    void add_auto(Func& function,std::string scope="default");
    void add_custom(Func& function,std::function<void()> callback);
    void schedule(Pipeline& pipeline);

};
#endif // SEMIAUTOSCHEDULE_H
