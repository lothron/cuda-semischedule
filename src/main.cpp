



#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <Halide.h>

#include "semiautoschedule.h"
#include "AutoSchedule.h"


Target find_gpu_target() {

    Target target = get_host_target();

    std::vector<Target::Feature> features_to_try;


    features_to_try.push_back(Target::CUDA);
    //Suche nur nach CUDA-Gpu's

    for (Target::Feature f : features_to_try) {
        Target new_target = target.with_feature(f).with_feature(Target::CUDACapability61);
        if (host_supports_target_device(new_target)) {
            return new_target;
        }
    }

    printf("Requested GPU(s) are not supported. (Do you have the proper hardware and/or driver installed?)\n");
    throw std::runtime_error("");
}


namespace impl {
    struct FatPipeline{
        Pipeline pipe;
        SemiScheduler scheduler;
    };
    Var x("x"), y("y"), c("c"), yi("yi"), yo("yo"), yii("yii"),xii("xii"), xi("xi"),k("k");

    Expr sum3x3(Func f, Var x, Var y) {
        return f(x - 1, y - 1) + f(x - 1, y) + f(x - 1, y + 1) +
               f(x, y - 1) + f(x, y) + f(x, y + 1) +
               f(x + 1, y - 1) + f(x + 1, y) + f(x + 1, y + 1);
    }
    Expr avg(Expr a, Expr b) {

        return (a+b)/2;
    }

    Expr blur121(Expr a, Expr b, Expr c) { return avg(avg(a, c), b); }

    Func interleave_x(Func a, Func b) {
        Func out;
        out(x, y) = select((x % 2) == 0, a(x / 2, y), b(x / 2, y));
        return out;
    }

    Func interleave_y(Func a, Func b) {
        Func out;
        out(x, y) = select((y % 2) == 0, a(x, y / 2), b(x, y / 2));
        return out;
    }

    struct Constants{
        float color_temp = 3700;
        float gamma = 2.0;
        float contrast = 50;
        float sharpen = 1.0;
        int timing_iterations = 5;
        int blackLevel = 25;
        int whiteLevel = 1023;
        Buffer<float> matrix_3200, matrix_7000;
        Constants():matrix_3200(4, 3), matrix_7000(4, 3){
            float _matrix_3200[][4] = {{1.6697f, -0.2693f, -0.4004f, -42.4346f},
                                   {-0.3576f, 1.0615f, 1.5949f, -37.1158f},
                                   {-0.2175f, -1.8751f, 6.9640f, -26.6970f}};

            float _matrix_7000[][4] = {{2.2997f, -0.4478f, 0.1706f, -39.0923f},
                                   {-0.3826f, 1.5906f, -0.2080f, -25.4311f},
                                   {-0.0888f, -0.7344f, 2.2832f, -20.0826f}};
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++) {
                    matrix_3200(j, i) = _matrix_3200[i][j];
                    matrix_7000(j, i) = _matrix_7000[i][j];
                }
            }

        }
    };


    struct HotPixelSuppression{
        Func output;
        HotPixelSuppression(Func input,SemiScheduler& info):output("supression"){
            Expr a = max(input(x - 2, y), input(x + 2, y),
                     input(x, y - 2), input(x, y + 2));

            output(x, y) = clamp(input(x, y), 0, a);
            info.add_auto(output);


        }

    };
    struct ColorCorrect{
        Func output;
        Func matrix;
        ColorCorrect(Func input,const Constants& con,SemiScheduler& info):output("ccorrect"){
            Expr kelvin = con.color_temp;


            Expr alpha = (1.0f / kelvin - 1.0f / 3200) / (1.0f / 7000 - 1.0f / 3200);
            Expr val = (con.matrix_3200(x, y) * alpha + con.matrix_7000(x, y) * (1 - alpha));
            matrix(x, y) = val;  // Q8.8 fixed point


                info.add_custom(matrix,[=](){
                    matrix.compute_root();
                    matrix.gpu_single_thread();
                });



            Expr ir = input(x, y, 0);
            Expr ig =input(x, y, 1);
            Expr ib = input(x, y, 2);
            Expr r = matrix(3, 0) + matrix(0, 0) * ir + matrix(1, 0) * ig + matrix(2, 0) * ib;
            Expr g = matrix(3, 1) + matrix(0, 1) * ir + matrix(1, 1) * ig + matrix(2, 1) * ib;
            Expr b = matrix(3, 2) + matrix(0, 2) * ir + matrix(1, 2) * ig + matrix(2, 2) * ib;

            output(x, y, c) =   mux(c, {r, g, b})/256.0f;
            info.add_auto(output);


        }
    };


    struct ColorCurve{
        Func curve;
        Func output;
        ColorCurve(Func input,const Constants& con,SemiScheduler& info):output("ccurve"),curve("lut"){

            Expr minRaw = 0 + con.blackLevel;
            Expr maxRaw = con.whiteLevel;

            // How much to upsample the LUT by when sampling it.
            int lutResample = 1;

            minRaw /= lutResample;
            maxRaw /= lutResample;

            Expr invRange = 1.0f / (maxRaw - minRaw);
            Expr b = 2.0f - pow(2.0f, Expr(con.contrast) / 100.0f);
            Expr a = 2.0f - 2.0f * b;

            // Get a linear luminance in the range 0-1
            Expr xf = clamp(cast<float>(x - minRaw) * invRange, 0.0f, 1.0f);
            // Gamma correct it
            Expr g = pow(xf, 1.0f / con.gamma);
            // Apply a piecewise quadratic contrast curve
            Expr z = select(g > 0.5f,
                            1.0f - (a * (1.0f - g) * (1.0f - g) + b * (1.0f - g)),
                            a * g * g + b * g);

            // Convert to 8 bit and save
            Expr val = clamp(z * 255.0f + 0.5f, 0.0f, 255.0f);
            // makeLUT add guard band outside of (minRaw, maxRaw]:
            curve(x) = select(x <= minRaw, 0.0f, select(x > maxRaw, 255.0f, val));


                // It's a LUT, compute it once ahead of time.

            info.add_custom(curve,[&](){
                curve.compute_root();
                curve.gpu_tile(x, xi, 32);
            });


            /* Optional tags to specify layout for HalideTraceViz */

            output(x, y, c) =  curve( clamp(cast<int>(input(x, y, c)*256.0f), 0, 1023));


            info.add_auto(output);



        }


    };

    struct Sharpen{

        Func unsharp_y;
        Func unsharp;
        Func output;
        Sharpen(Func input,const Constants& con,SemiScheduler& info):output("sharpen"),unsharp("unsharp"),unsharp_y("unsharp_y"){


            unsharp_y(x, y, c) = blur121(input(x, y - 1, c), input(x, y, c), input(x, y + 1, c));
            unsharp(x, y, c) = blur121(unsharp_y(x - 1, y, c), unsharp_y(x, y, c), unsharp_y(x + 1, y, c));

            Func mask("mask");
            mask(x, y, c) = input(x, y, c) - unsharp(x, y, c);

            // Weight the mask with the sharpening strength, and add it to the
            // input to get the sharpened result.

            output(x, y, c) = input(x, y, c) + (mask(x, y, c) * con.sharpen) ;
            info.add_auto(output);




        }
    };


    struct Deinterleave{
        Func output;
        Deinterleave(Func raw,SemiScheduler& info):output("deinterleave"){

            output(x,y,c)= mux(c,
                                     {raw(x*2, y*2),
                                      raw( x*2 + 1,  y*2),
                                      raw(x*2, y*2 + 1),
                                      raw(x*2 + 1,  y*2 + 1)});

            info.add_auto(output);





        }


    };





    struct Debayer{

        Func g_r;
        Func g_b;
        Func output;
        Debayer(const Func& deinterleaved,SemiScheduler& info ):g_r("g_r"),g_b("g_b"),output("debayer"){
            Func r_r, g_gr, g_gb, b_b;
            g_gr(x, y) = deinterleaved(x, y, 0);
            r_r(x, y) = deinterleaved(x, y, 1);
            b_b(x, y) = deinterleaved(x, y, 2);
            g_gb(x, y) = deinterleaved(x, y, 3);

            Func b_r, b_gr, r_gr, b_gb, r_gb, r_b;

            // First calculate green at the red and blue sites

            // Try interpolating vertically and horizontally. Also compute
            // differences vertically and horizontally. Use interpolation in
            // whichever direction had the smallest difference.
            Expr gv_r = avg(g_gb(x, y - 1), g_gb(x, y));
            Expr gvd_r = absd(g_gb(x, y - 1), g_gb(x, y));
            Expr gh_r = avg(g_gr(x + 1, y), g_gr(x, y));
            Expr ghd_r = absd(g_gr(x + 1, y), g_gr(x, y));

            g_r(x, y) = select(ghd_r < gvd_r, gh_r, gv_r);

            Expr gv_b = avg(g_gr(x, y + 1), g_gr(x, y));
            Expr gvd_b = absd(g_gr(x, y + 1), g_gr(x, y));
            Expr gh_b = avg(g_gb(x - 1, y), g_gb(x, y));
            Expr ghd_b = absd(g_gb(x - 1, y), g_gb(x, y));

            g_b(x, y) = select(ghd_b < gvd_b, gh_b, gv_b);

            // Next interpolate red at gr by first interpolating, then
            // correcting using the error green would have had if we had
            // interpolated it in the same way (i.e. add the second derivative
            // of the green channel at the same place).
            Expr correction;
            correction = g_gr(x, y) - avg(g_r(x, y), g_r(x - 1, y));
            r_gr(x, y) = correction + avg(r_r(x - 1, y), r_r(x, y));

            // Do the same for other reds and blues at green sites
            correction = g_gr(x, y) - avg(g_b(x, y), g_b(x, y - 1));
            b_gr(x, y) = correction + avg(b_b(x, y), b_b(x, y - 1));

            correction = g_gb(x, y) - avg(g_r(x, y), g_r(x, y + 1));
            r_gb(x, y) = correction + avg(r_r(x, y), r_r(x, y + 1));

            correction = g_gb(x, y) - avg(g_b(x, y), g_b(x + 1, y));
            b_gb(x, y) = correction + avg(b_b(x, y), b_b(x + 1, y));

            // Now interpolate diagonally to get red at blue and blue at
            // red. Hold onto your hats; this gets really fancy. We do the
            // same thing as for interpolating green where we try both
            // directions (in this case the positive and negative diagonals),
            // and use the one with the lowest absolute difference. But we
            // also use the same trick as interpolating red and blue at green
            // sites - we correct our interpolations using the second
            // derivative of green at the same sites.

            correction = g_b(x, y) - avg(g_r(x, y), g_r(x - 1, y + 1));
            Expr rp_b = correction + avg(r_r(x, y), r_r(x - 1, y + 1));
            Expr rpd_b = absd(r_r(x, y), r_r(x - 1, y + 1));

            correction = g_b(x, y) - avg(g_r(x - 1, y), g_r(x, y + 1));
            Expr rn_b = correction + avg(r_r(x - 1, y), r_r(x, y + 1));
            Expr rnd_b = absd(r_r(x - 1, y), r_r(x, y + 1));

            r_b(x, y) = select(rpd_b < rnd_b, rp_b, rn_b);

            // Same thing for blue at red
            correction = g_r(x, y) - avg(g_b(x, y), g_b(x + 1, y - 1));
            Expr bp_r = correction + avg(b_b(x, y), b_b(x + 1, y - 1));
            Expr bpd_r = absd(b_b(x, y), b_b(x + 1, y - 1));

            correction = g_r(x, y) - avg(g_b(x + 1, y), g_b(x, y - 1));
            Expr bn_r = correction + avg(b_b(x + 1, y), b_b(x, y - 1));
            Expr bnd_r = absd(b_b(x + 1, y), b_b(x, y - 1));

            b_r(x, y) = select(bpd_r < bnd_r, bp_r, bn_r);

            // Resulting color channels
            Func r, g, b;

            // Interleave the resulting channels
            r = interleave_y(interleave_x(r_gr, r_r), interleave_x(r_b, r_gb));
            g = interleave_y(interleave_x(g_gr, g_r), interleave_x(g_b, g_gb));
            b = interleave_y(interleave_x(b_gr, b_r), interleave_x(b_b, b_gb));

            output(x, y, c) = mux(c, {r(x, y), g(x, y), b(x, y)});
            info.add_auto(output);;
            info.add_auto(g_b);
            info.add_auto(g_r);




        }



    };
    struct Boundary{
        Func output;
        Boundary(Buffer<float>& input,SemiScheduler& info){

            output(x, y) = BoundaryConditions::repeat_edge( input)(x,y);


        }

    };
    struct BoundaryRGB{
        Func output;
        BoundaryRGB(Buffer<float>& input,SemiScheduler& info){

            output(x, y,c) = BoundaryConditions::repeat_edge( input)(x,y,c);


        }

    };


    std::vector<Expr> gen_sort_net_expr(std::vector<Expr> values){

        int N = values.size();
        for(int p =1;p<N; p *=2){
            for(int k =p;k>=1; k/=2){
                for(int j = k%p; j<=N-1-k; j +=2*k){
                    for(int i = 0; i<=k-1;i++){
                        if((i+j)/(p*2) ==(i+k+j)/(p*2) &&i+j+k<N){
                            Expr a =min(values[i+j], values[(i+j+k)]);
                            Expr b =max(values[i+j], values[(i+j+k)]);
                            values[i+j] =a;
                            values[i+j+k] =b;

                        }
                    }
                }
            }
        }
        return  values;

    }


    struct Median{
        Func output;
        Median(Func in,SemiScheduler& info):output("median"){
            std::vector<Expr> samples;
            int tile = 3;
            for(int i =0;i<tile;i++){
                for(int j =0;j<tile;j++){

                    samples.push_back(in(x+int(-tile/2+i),y+int(-tile/2+j),c));
                }
            }
            output(x,y,c) = gen_sort_net_expr(samples)[tile*tile/2];

            info.add_auto(output);;




        }


    };
    struct RGBHist{
        RDom r;
        Func  output;
        RGBHist(Func input,Expr with,Expr height,SemiScheduler& info):output("hist"){

            output(x,c) = 0;
            r= RDom(0,with,0,height,"r");

            Expr value = cast<int>( input(r.x,r.y,c)*255)%256;
            output(value,c) +=1;
            info.add_custom(output,[=](){
                RVar ri,rx;
                Func intermediate = output.update().rfactor({{r.y, y}});
                intermediate.in().compute_root().gpu_threads(x).reorder(y,c).gpu_blocks(y,c);
                intermediate.compute_at(intermediate.in(),y).reorder(x,c).gpu_threads(x,c).update().atomic().split(r.x,rx,ri,16).reorder(ri,rx,y,c).gpu_threads(rx,y,c);
                output.compute_root().gpu_tile(x,xi,32).gpu_blocks(c).update().reorder(x,c).gpu_tile(x,xi,32).gpu_blocks(c);
            });
        }
    };
    struct Decompand{
        Func output;
        Decompand(Func input,SemiScheduler& info):output("decompand"){

            Expr linear("linear");
            Expr blackOffset("blackoffset");
            blackOffset = input(x, y) - static_cast<float>(0.1);
            for (auto i = 1; i < 8; i++) {
                if (i == 1) {
                    linear = select(blackOffset < 0, blackOffset,blackOffset*(1.0f/(i+1)));
                } else {
                    float gain = (1.0f/(i+1));

                    Expr y_off("y_off");
                    y_off = blackOffset - (float)(2.0f/(i+1));

                    linear = select(
                      y_off < 0, linear,
                      y_off * gain + (float)(1.0f/(i+1)));
                }
            }
            output(x, y) = linear;
            info.add_auto(output);

        }
    };


    void test_pipline(int stage,int mode){
        Buffer<float> input = Halide::Tools::load_and_convert_image("test.png");
        Buffer<float> output_rgb(input.width() , input.height(),3);
        Buffer<float> output_gray(input.width() , input.height());
        Buffer<int> hist_output(256,3);
        using namespace impl;
        SemiScheduler scheduler;

        Constants constants;
        Boundary boundary(input,scheduler);
        Decompand decompand(boundary.output,scheduler);
        HotPixelSuppression supression(decompand.output,scheduler);
        Deinterleave deinterleave(supression.output,scheduler);
        Debayer deb(deinterleave.output,scheduler);
        Median median (deb.output,scheduler);
        ColorCorrect ccorrect(median.output,constants,scheduler);
        ColorCurve  ccurve(ccorrect.output,constants,scheduler);
        Sharpen sharpen(ccurve.output,constants,scheduler);
        RGBHist hist(sharpen.output,output_rgb.width(),output_rgb.height(),scheduler);
        Target tgt= find_gpu_target();
        Pipeline pipe;
        switch (stage) {
        case 0:
            decompand.output.set_estimate(x,0, 1<<12);
            decompand.output.set_estimate(y,0, 1<<12);

            pipe =Pipeline({decompand.output});
            break;
        case 1:
            supression.output.set_estimate(x,0, 1<<12);
            supression.output.set_estimate(y,0, 1<<12);
            pipe =Pipeline({supression.output});
            break;
        case 2:
            deb.output.set_estimate(x,0, 1<<12);
            deb.output.set_estimate(y,0, 1<<12);
            deb.output.set_estimate(c,0, 3);
            pipe =Pipeline({deb.output});

            break;
        case 3:
            median.output.set_estimate(x,0, 1<<12);
            median.output.set_estimate(y,0, 1<<12);
            median.output.set_estimate(c,0, 3);
            pipe =Pipeline({median.output});

            break;
        case 4:
            ccorrect.output.set_estimate(x,0, 1<<12);
            ccorrect.output.set_estimate(y,0, 1<<12);
            ccorrect.output.set_estimate(c,0, 3);
            pipe =Pipeline({ccorrect.output});

            break;
        case 5:
            ccurve.output.set_estimate(x,0, 1<<12);
            ccurve.output.set_estimate(y,0, 1<<12);
            ccurve.output.set_estimate(c,0, 3);
            pipe =Pipeline({ccurve.output});

            break;
        case 6:
            sharpen.output.set_estimate(x,0, 1<<12);
            sharpen.output.set_estimate(y,0, 1<<12);
            sharpen.output.set_estimate(c,0, 3);
            pipe =Pipeline({sharpen.output});

            break;
        case 7:
            sharpen.output.set_estimate(x,0, 1<<12);
            sharpen.output.set_estimate(y,0, 1<<12);
            sharpen.output.set_estimate(c,0, 3);
            hist.output.set_estimate(x, 0,255);
            hist.output.set_estimate(c, 0,3);
            pipe =Pipeline({sharpen.output,hist.output});

            break;
        }
        std::vector<Buffer<float>> outputs;


        pipe.outputs()[0].bound(x,0,(output_rgb.width()*2)/2);
        pipe.outputs()[0].bound(y,0,(output_rgb.height()*2)/2);

        if(stage>1){
            pipe.outputs()[0].bound(c,0,3);
        }
        if(mode==-1){

            scheduler.scopes["default"] = FScope{{2,1,1},{3,2,2},512};
            scheduler.schedule(pipe);

        }
        if(mode==0){
            scheduler.scopes["default"] = FScope{{1,2,1},{3,2,2},512};
            scheduler.schedule(pipe);

        }else if(mode==1){

            decompand.output.compute_root().gpu_tile(x,y,xi,yi,32,16);
            sharpen.output.compute_root().reorder(c,x,y).bound(c, 0,3).unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            //test.output.compute_root().reorder(c,x,y).bound(c, 0,3).unroll(x,2).gpu_tile(x,y,xi,yi,32,16).update().reorder(c,x,y).unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            ccurve.output.compute_root().reorder(c,x,y).bound(c, 0,3).unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            ccorrect.output.compute_root().reorder(c,x,y).bound(c, 0,3).unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            median.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            deb.g_b.compute_at(deb.output,x).gpu_threads(x,y);
            deb.g_r.compute_at(deb.output,x).gpu_threads(x,y);
            deb.output.compute_root().reorder(c,x,y).bound(c, 0,3).unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            deinterleave.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            supression.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            RVar ri,rx;
            Func intermediate = hist.output.update().rfactor({{hist.r.y, y}});
            intermediate.in().compute_root().gpu_threads(x).reorder(y,c).gpu_blocks(y,c);
            intermediate.compute_at(intermediate.in(),y).reorder(x,c).gpu_threads(x,c).update().atomic().split(hist.r.x,rx,ri,16).reorder(ri,rx,y,c).gpu_threads(rx,y,c);
            hist.output.compute_root().gpu_tile(x,xi,32).gpu_blocks(c).update().reorder(x,c).gpu_tile(x,xi,32).gpu_blocks(c);

        }
        else if(mode==2){


            pipe.auto_schedule("Sioutas20",tgt);
            if(stage == 7){
                std::cout<<"no recursive support"<<std::endl;
                return;
            }

        }
        else if(mode==3){


            pipe.auto_schedule("Li2018",tgt);


        }
        pipe.compile_jit(tgt);
        auto best = Halide::Tools::benchmark(   10, 15, [&]() {
            if(stage<2){
                pipe.realize(output_gray,tgt);
                output_gray.device_sync();
            }
            else if(stage<7){
               pipe.realize(output_rgb,tgt);
             output_rgb.device_sync();
            }
            else if(stage==7){
                pipe.realize({output_rgb,hist_output},tgt);
                 output_rgb.device_sync();
                hist_output.device_sync();

            }


        });
        std::cout<<best<<std::endl;
    }
    struct Laplace{
        Func output;
        std::pair<Func,Func> downsample(Func f,std::string name) {

            Func downx(name+"downx"), downy(name+"downy");
            downx(x, y, _) = (f(2 * x - 1, y, _) + 3.0f * (f(2 * x, y, _) + f(2 * x + 1, y, _)) + f(2 * x + 2, y, _)) / 8.0f;
            downy(x, y, _) = (downx(x, 2 * y - 1, _) + 3.0f * (downx(x, 2 * y, _) + downx(x, 2 * y + 1, _)) + downx(x, 2 * y + 2, _)) / 8.0f;
            return {downx,downy};
        }
        // Upsample using bilinear interpolation
        std::pair<Func,Func> upsample(Func f,std::string name) {

            Func upx(name+"_upy"), upy(name+"_upy");
            upx(x, y, _) = lerp(f((x + 1) / 2, y, _), f((x - 1) / 2, y, _), ((x % 2) * 2 + 1) / 4.0f);
            upy(x, y, _) = lerp(upx(x, (y + 1) / 2, _), upx(x, (y - 1) / 2, _), ((y % 2) * 2 + 1) / 4.0f);
            return {upx,upy};
        }
        static constexpr int maxJ = 20;
        Func outLPyramid[maxJ];

        Func gPyramid[maxJ];
        Func gPyramid_pre[maxJ];

        Func inGPyramid[maxJ];
        Func inGPyramid_pre[maxJ];
        Func outGPyramid[maxJ];
        Func outGPyramid_pre[maxJ];
        Func lPyramid[maxJ];
        Func lPyramid_pre[maxJ];
        Func remap;
        Func gray;
        Laplace(Func input,SemiScheduler& info):remap("remap"),output("output"),gray("gray"){
            Expr fx = cast<float>(x) / 256.0f;
            for(int i = 0;i<maxJ;i++){
                gPyramid[i]= Func("gPyramide"+std::to_string(i));
                inGPyramid[i]= Func("outGPyramide"+std::to_string(i));
                outGPyramid[i]= Func("inGPyramide"+std::to_string(i));
                outLPyramid[i]= Func("inLPyramide"+std::to_string(i));
                lPyramid[i]= Func("lPyramide"+std::to_string(i));
            }

            remap(x) =0.8f * fx * exp(-fx * fx / 2.0f);
            info.add_custom(remap,[this](){
                remap.compute_root().gpu_single_thread();
            });
            const int J = 8;
            const int levels = 8;
            const float beta =0.1f;
            Func floating("fl");
            floating(x, y, c) = input(x, y, c);

            // Get the luminance channel

            gray(x, y) = 0.299f * floating(x, y, 0) + 0.587f * floating(x, y, 1) + 0.114f * floating(x, y, 2);


            // Do a lookup into a lut with 256 entires per intensity level
            Expr level = k * (1.0f / (levels - 1));
            Expr idx = gray(x, y) * cast<float>(levels - 1) * 256.0f;
            idx = clamp(cast<int>(idx), 0, (levels - 1) * 256);
            gPyramid[0](x, y, k) = beta * (gray(x, y) - level) + level + remap(idx - 256 * k);
            for (int j = 1; j < J; j++) {
                auto [dx,dy] = downsample(gPyramid[j - 1], gPyramid[j].name());
                gPyramid_pre[j] = dx;
                gPyramid[j](x, y, k) = dy(x, y, k);

            }
            // Get its laplacian pyramid

            lPyramid[J - 1](x, y, k) = gPyramid[J - 1](x, y, k);
            for (int j = J - 2; j >= 0; j--) {
                auto [ux,uy] = upsample(gPyramid[j + 1], lPyramid[j].name());
                lPyramid_pre[j] = ux;
                lPyramid[j](x, y, k) = gPyramid[j](x, y, k) - uy(x, y, k);
            }

            // Make the Gaussian pyramid of the input

            inGPyramid[0](x, y) = gray(x, y);
            for (int j = 1; j < J; j++) {
                auto [dx,dy] = downsample(inGPyramid[j - 1],inGPyramid[j].name());
                inGPyramid_pre[j]=dx;
                inGPyramid[j](x, y) =dy(x, y);
            }

            // Make the laplacian pyramid of the output

            for (int j = 0; j < J; j++) {
                // Split input pyramid value into integer and floating parts
                Expr level = inGPyramid[j](x, y) * cast<float>(levels - 1);
                Expr li = clamp(cast<int>(level), 0, levels - 2);
                Expr lf = level - cast<float>(li);
                // Linearly interpolate between the nearest processed pyramid levels
                outLPyramid[j](x, y) = (1.0f - lf) * lPyramid[j](x, y, li) + lf * lPyramid[j](x, y, li + 1);
            }

            // Make the Gaussian pyramid of the output

            outGPyramid[J - 1](x, y) = outLPyramid[J - 1](x, y);
            for (int j = J - 2; j >= 0; j--) {
                auto [dx,dy] = upsample(outGPyramid[j + 1], outGPyramid[j].name());
                outGPyramid_pre[j] = dx;
                outGPyramid[j](x, y) = dy(x, y) + outLPyramid[j](x, y);
            }
            info.add_auto(gray);
            for (int j = 0; j < 8; j++) {


                if(j>0&&j<7){

                 
                    info.add_auto(outGPyramid[j],"complex");
                    info.add_auto(gPyramid[j]);
                    info.add_auto(inGPyramid[j],"complex");
                    

               }



           }

            float eps = 0.01f;
            output(x, y, c) = clamp(outGPyramid[0](x, y) * (floating(x, y, c) + eps) / (gray(x, y) + eps),0.0f,1.0f);

            // Convert back to 16-bit
            info.add_auto(output);






        }
    };

    void test_laplace(int mode){
        Buffer<float> input = Halide::Tools::load_and_convert_image("parrot_final.jpg");
        Buffer<float> output_rgb(input.width() , input.height(),3);
        using namespace impl;
        SemiScheduler scheduler;
        BoundaryRGB boundary(input,scheduler);
        Laplace laplace(boundary.output,scheduler);
        Pipeline pipe(laplace.output);
        laplace.output.set_estimates({{0,1<<12},{0,1<<12},{0,3}});

        laplace.output.bound(c,0,3);
        laplace.output.bound(x,0,output_rgb.width());
        laplace.output.bound(y,0,output_rgb.height());
        Target tgt= find_gpu_target();
        if(mode==-1){
            std::cout<<"Naive"<<std::endl;
            laplace.remap.compute_root().gpu_single_thread();
            laplace.gray.compute_root().gpu_tile(x, y, xi, yi, 32, 16);
            for (int j = 0; j < 8; j++) {
               int blockw = 16, blockh = 8;




               laplace.outGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);
               laplace.lPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);
               laplace.outLPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);
               laplace.gPyramid[j].compute_root().reorder(k,x,y).gpu_tile(x, y, xi, yi, blockw, blockh);
               laplace.inGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);




            }
            laplace.output.compute_root().unroll(c).reorder(c,x,y).gpu_tile(x, y, xi, yi, 32, 16);


        }
        if(mode==0){
            std::cout<<"Semi"<<std::endl;
            scheduler.scopes["default"] = FScope{{1,1,1},{1,1,1},256};
            scheduler.scopes["complex"] = FScope{{1,1,1},{1,1,1},128};
            scheduler.schedule(pipe);
        }
        if(mode==1){
            std::cout<<"Step1"<<std::endl;
            laplace.remap.compute_root().gpu_single_thread();
            //laplace.gray.compute_root().gpu_tile(x, y, xi, yi, 32, 16);
            for (int j = 0; j < 8; j++) {
               int blockw = 16, blockh = 8;


               if(j>6){
                   blockh = 4;
                   blockw=4;

               }
               if(j>0&&j<7){


                   laplace.outGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);

                   laplace.gPyramid[j].compute_root().reorder(k,x,y).gpu_tile(x, y, xi, yi, blockw, blockh);

                   laplace.inGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);
               }



           }

           laplace.output.compute_root().unroll(c).reorder(c,x,y).gpu_tile(x, y, xi, yi, 32, 16);
        }
        if(mode==2){
             std::cout<<"Step3"<<std::endl;
            laplace.remap.compute_root().gpu_single_thread();
            //laplace.gray.compute_root().gpu_tile(x, y, xi, yi, 32, 16);
            for (int j = 0; j < 8; j++) {
               int blockw = 16, blockh = 8;


               if(j>6){
                   blockh = 4;
                   blockw=4;

               }
               if(j>0&&j<7){


                   laplace.outGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh);
                   laplace.gPyramid_pre[j].compute_at(laplace.gPyramid[j],x).reorder(_0,x,y).gpu_threads(x,y);
                   laplace.gPyramid[j].compute_root().reorder(k,x,y).gpu_tile(x, y, xi, yi, blockw, blockh-2);
                   laplace.inGPyramid_pre[j].compute_at(laplace.inGPyramid[j],x).gpu_threads(x,y);
                   laplace.inGPyramid[j].compute_root().gpu_tile(x, y, xi, yi, blockw, blockh-2);
               }



           }

           laplace.output.compute_root().unroll(c).reorder(c,x,y).gpu_tile(x, y, xi, yi, 16, 8);
        }
        if(mode==3){
            std::cout<<"Sioutas20"<<std::endl;
            pipe.auto_schedule("Sioutas20",tgt);
        }
        if(mode==4){
            std::cout<<"Li2018"<<std::endl;
            pipe.auto_schedule("Li2018",tgt);
        }
        auto best = Halide::Tools::benchmark( [&]() {

            pipe.realize(output_rgb,tgt);


            output_rgb.device_sync();
        });
        std::cout<<best<<std::endl;

    }
    struct Harris{
        Func output;
        Func gray;
        Func Iy;
        Func Ix;
        Func Ixx;
        Func Ixy;
        Func Iyy;
        Func Sxx;
        Func Sxy;
        Func Syy;
        Func det;
        Func trace;
        Harris(Func input,SemiScheduler& scheduler){
            Var x("x"), y("y"), c("c");

           // Algorithm

           gray(x, y) = (0.299f * input(x, y, 0) +
                         0.587f * input(x, y, 1) +
                         0.114f * input(x, y, 2));


           Iy(x, y) = gray(x - 1, y - 1) * (-1.0f / 12) + gray(x - 1, y + 1) * (1.0f / 12) +
                      gray(x, y - 1) * (-2.0f / 12) + gray(x, y + 1) * (2.0f / 12) +
                      gray(x + 1, y - 1) * (-1.0f / 12) + gray(x + 1, y + 1) * (1.0f / 12);


           Ix(x, y) = gray(x - 1, y - 1) * (-1.0f / 12) + gray(x + 1, y - 1) * (1.0f / 12) +
                      gray(x - 1, y) * (-2.0f / 12) + gray(x + 1, y) * (2.0f / 12) +
                      gray(x - 1, y + 1) * (-1.0f / 12) + gray(x + 1, y + 1) * (1.0f / 12);


           Ixx(x, y) = Ix(x, y) * Ix(x, y);


           Iyy(x, y) = Iy(x, y) * Iy(x, y);


           Ixy(x, y) = Ix(x, y) * Iy(x, y);


           Sxx(x, y) = sum3x3(Ixx, x, y);


           Syy(x, y) = sum3x3(Iyy, x, y);


           Sxy(x, y) = sum3x3(Ixy, x, y);


           det(x, y) = Sxx(x, y) * Syy(x, y) - Sxy(x, y) * Sxy(x, y);


           trace(x, y) = Sxx(x, y) + Syy(x, y);
           output(x, y) = det(x, y) - 5.0f * trace(x, y) * trace(x, y);
           scheduler.add_auto(output);
           scheduler.add_auto(gray);
           scheduler.add_auto(Ix);
           scheduler.add_auto(Iy);
        }
    };
    void test_harris(int mode){
        Buffer<float> input = Halide::Tools::load_and_convert_image("parrot_final.jpg");
        Buffer<float> output_rgb(input.width() , input.height());
        using namespace impl;
        SemiScheduler scheduler;
        BoundaryRGB boundary(input,scheduler);
        Harris harris(boundary.output,scheduler);
        Pipeline pipe(harris.output);
        harris.output.set_estimates({{0,1<<11},{0,1<<11}});

        Target tgt= find_gpu_target();
        if(mode==-1){
            std::cout<<"Naive"<<std::endl;
            harris.gray.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Ix.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Iy.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.output.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Ixx.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Ixy.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Iyy.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Sxx.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Sxy.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Syy.compute_root().gpu_tile(x,y,xi,yi,32,16);
        }
        if(mode==0){
            std::cout<<"Step1"<<std::endl;
            harris.gray.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Ix.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.Iy.compute_root().gpu_tile(x,y,xi,yi,32,16);
            harris.output.compute_root().gpu_tile(x,y,xi,yi,32,16);

        }
        if(mode==1){
            std::cout<<"Step2"<<std::endl;
            harris.gray.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            harris.Ix.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            harris.Iy.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
            harris.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,32,16);
        }
        if(mode==2){
            std::cout<<"Step3"<<std::endl;
            harris.gray.compute_at(harris.output,x).tile(x,y,xii,yii,3,2).unroll(xii).unroll(yii).gpu_threads(x,y);
            harris.Ix.compute_at(harris.output,x).unroll(x,2).gpu_threads(x,y);
            harris.Iy.compute_at(harris.output,x).unroll(x,2).gpu_threads(x,y);
            harris.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,31,14);
        }
        if(mode==3){
            std::cout<<"Step4"<<std::endl;
            harris.gray.compute_at(harris.output,x).tile(x,y,xii,yii,3,2).unroll(xii).unroll(yii).gpu_threads(x,y);
            harris.gray.in().compute_at(harris.Iy,x).unroll(x).unroll(y);
            harris.Ix.compute_at(harris.output,x).unroll(x,2).gpu_threads(x,y);
            harris.Iy.compute_at(harris.output,x).unroll(x,2).gpu_threads(x,y);
            harris.Ix.compute_with(harris.Iy,x);
            harris.output.compute_root().unroll(x,2).gpu_tile(x,y,xi,yi,31,14);


        }
        if(mode==4){
            scheduler.scopes["default"] = FScope{{1,2,1},{3,2,2},512};
            std::cout<<"Semi"<<std::endl;
            scheduler.schedule(pipe);
        }
        if(mode==5){
            std::cout<<"Sioutas20"<<std::endl;
            pipe.auto_schedule("Sioutas20",tgt);
        }
        if(mode==7){
            std::cout<<"Li2018"<<std::endl;
            pipe.auto_schedule("Li2018",tgt);
        }
        harris.output.compile_jit(tgt);
        auto best = Halide::Tools::benchmark([&]() {

            pipe.realize(output_rgb,tgt);

            output_rgb.device_sync();
        });
        std::cout<<best<<std::endl;

    }

}


void bench_harris(){
    std::cout<<"Harris"<<std::endl;
    for(int mode =-1;mode<=7;mode++){
        impl::test_harris(mode);

    }
}

void bench_pipeline(){
    std::cout<<"Testpipeline"<<std::endl;

    for(int mode =-1;mode<4;mode++){
        if(mode==-1){
            std::cout<<"Semi:Scope1"<<std::endl;
        }
        if(mode==0){
            std::cout<<"Semi:Scope2"<<std::endl;
        }
        if(mode==1){
            std::cout<<"Manual"<<std::endl;
        }
        if(mode==2){
            std::cout<<"Sioutas2020"<<std::endl;
        }
        if(mode==3){
            std::cout<<"Li2018"<<std::endl;
        }
        for(int stage =0;stage<8;stage++){
            impl::test_pipline(stage,mode);
        }

    }
}
void bench_laplace(){
    std::cout<<"Laplace"<<std::endl;
    Buffer<float> input = Halide::Tools::load_and_convert_image("parrot_final.jpg");
    Buffer<float> output_rgb(input.width() , input.height(),3);
    using namespace impl;
    SemiScheduler scheduler;
    BoundaryRGB boundary(input,scheduler);
    Laplace laplace(boundary.output,scheduler);
    for(int mode =-1;mode<=4;mode++){
        impl::test_laplace(mode);
    }


}





int main(int argc, char *argv[])
{
    RegisterSioutas2020();
    RegisterLi2018();
    
    if(std::strcmp(argv[1],"harris")==0){
        bench_harris();
    }
    if(std::strcmp(argv[1],"laplace")==0){
        bench_laplace();
    }
    if(std::strcmp(argv[1],"pipeline")==0){
        bench_pipeline();
    }
}
