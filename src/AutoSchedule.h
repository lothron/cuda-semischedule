#ifndef AUTOSCHEDULE_H
#define AUTOSCHEDULE_H

#include "Halide.h"
using namespace Halide;
using namespace Internal;
struct RegisterSioutas2020 {
    RegisterSioutas2020() {

        Pipeline::add_autoscheduler("Sioutas20", *this);
    }

    void operator()(const Pipeline &p, const Target &target, const MachineParams &params, AutoSchedulerResults *results);
};
struct RegisterLi2018 {
     RegisterLi2018() {

        Pipeline::add_autoscheduler("Li2018", *this);
    }

    void operator()(const Pipeline &p, const Target &target, const MachineParams &params, AutoSchedulerResults *results);
};
#endif // AUTOSCHEDULE_H
