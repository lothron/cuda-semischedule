#include "semiautoschedule.h"
#include <optional>




struct SemiScheduler;

//Taken from the orignal Halide Repo.
//Get the Function bounds for a given setup
const dag::Bound & get_bounds(dag::FunctionDAG::Node *f,std::vector<dag::Bound>& bounds,const dag::FunctionDAG& dag)  {
    if (bounds[f->id].get() != nullptr) {
        const dag::Bound &b = bounds[f->id];
        // Expensive validation for debugging
        // b->validate();
        return b;
    }
    auto *bound = f->make_bound();

    // Compute the region required
    if (f->is_output) {

        // It's an output. Use the bounds estimate.
        for (int i = 0; i < f->dimensions; i++) {
            bound->region_required(i) = f->estimated_region_required[i];
        }
    } else {

        auto init = dag::Span::empty_span();
        for (int i = 0; i < f->dimensions; i++) {
            bound->region_required(i) = init;
        }

        for (const auto *e : f->outgoing_edges) {
            // Ignore consumers outside of this loop nest

            const auto &c_bounds = get_bounds(e->consumer->node,bounds,dag);

            // Get the concrete sizes of the consuming loop
            const auto *consumer_loop = &(c_bounds->loops(e->consumer->index, 0));

            // Use the bounds relationship between the nodes to
            // map from the consumer's loop to the required region
            // of the producer.
            e->expand_footprint(consumer_loop, &(bound->region_required(0)));
        }
    }

    // Given a required region of this producer, use the bounds
    // analysis to figure out what region actually gets
    // computed. For most funcs, these are the same. Some things,
    // like histograms or scans, you can only really compute all
    // of at once.
    f->required_to_computed(&(bound->region_required(0)), &(bound->region_computed(0)));

    // Finally, figure out what loop nests will be used to compute
    // this region.
    for (int i = 0; i < (int)f->stages.size(); i++) {
        f->loop_nest_for_region(i, &(bound->region_computed(0)), &(bound->loops(i, 0)));
    }

    bounds[f->id] = bound;
    // Validation is expensive, turn if off by default.
    // b->validate();
    return bounds[f->id];
}

struct CCosts{
    double arith;
    double bytes_loaded;
};


using FInfo = dag::FunctionDAG::Node;
using SInfo = dag::FunctionDAG::Node::Stage;
using Span = dag::Span;
using FBound= dag::Bound;
using dag::FunctionDAG;


struct Occupancy{
    bool limited_by_shared_memory;
    bool limited_by_registers;
    bool limited_by_max_warps;
    float occupancy;
};

int ceilling(int val,int sig){
    float multiple = val/(float)sig;
    int remainder = val%sig;
    if(remainder==0){
        return val;
    }
    else{
        return ((int)std::ceil(multiple))*sig;
    }
}
int floor(int val,int sig){
    float multiple = val/(float)sig;
    int remainder = val%sig;
    if(remainder==0){
        return val;
    }
    else{
        return ((int)std::floor(multiple))*sig;
    }
}

double RoundAwayFromZero(double v) {
    auto s = std::copysign(1.0, v);
    return s * std::ceil(v * s);
}
//Get the theoretical occupancy w.r.t thread count , registers and shared memory
Occupancy estimate_occupancy(int my_thread_count,int my_registers_per_thread,int my_shared_memory){
    //Fixed values for a turing GPU
    //TODO add other hardware
    int threads_per_warp = 32;
    int warps_per_processor = 32;
    int blocks_per_processor = 1024;
    int max_block_size = 1024;
    int registers_per_processor = 1<<16;
    int registers_per_block = 1<<16;
    int registers_per_thread = 255;
    int sm_per_processor = 1<<16;
    int sm_per_block = 1<<16;
    int sm_allocation_size = 256;
    int register_allocation_size=256;
    int warp_granularity=4;
    int my_warps_per_block =std::ceil(my_thread_count/(float)threads_per_warp);
    int limits_due_warps = std::min(blocks_per_processor , warps_per_processor/my_warps_per_block);
    int my_regs_per_block = my_warps_per_block;
    int my_regs_per_processor = floor(registers_per_block/ceilling(my_registers_per_thread*threads_per_warp,register_allocation_size),warp_granularity);

    int limits_due_to_registers=-1;
    if(my_registers_per_thread>registers_per_thread){
        limits_due_to_registers=0;
    }
    else if(my_registers_per_thread>0){
        limits_due_to_registers = (int)(float(my_regs_per_processor)/my_regs_per_block)*(int)(float(registers_per_processor)/registers_per_block);
    }
    else{
        limits_due_to_registers = blocks_per_processor;
    }

    int my_shared_memory_per_block = ceilling(my_shared_memory,sm_allocation_size);
    int limits_due_to_sm;
    if(my_shared_memory_per_block>sm_per_processor){
        limits_due_to_sm = 0;
    }
    else{
        if(my_shared_memory_per_block==0){
            limits_due_to_sm = blocks_per_processor;
        }
        else{
            limits_due_to_sm= float(sm_per_processor)/my_shared_memory_per_block;
        }
    }

    if(limits_due_warps<=limits_due_to_sm&&limits_due_warps<=limits_due_to_registers){
        Occupancy o;
        o.limited_by_max_warps = true;
        o.occupancy = float(warps_per_processor)/ (my_warps_per_block*limits_due_warps);
        return o;
    }
    if(limits_due_to_registers<=limits_due_to_sm){
        Occupancy o;
        o.limited_by_registers = true;
        o.occupancy = float(warps_per_processor)/ (my_warps_per_block*limits_due_to_registers);
        return o;
    }
    Occupancy o;
    o.limited_by_shared_memory = true;

    o.occupancy =(my_warps_per_block*limits_due_to_sm)/float(warps_per_processor);
    return o;
};

//The tiling for Group
struct StageTiling{
    std::vector<std::vector<int>> inner_tiling;
    std::vector<int> outer_tiling;
    int threads=1;
    int shread_memory=0;
    bool valid=true;
};
//Meta info about a function stage
struct StageTilingInfo{
    struct Loop{
        Span range;
        int pure_dim;
    };
    std::vector<Loop> loops;
    std::vector<std::vector<int>> blocksizes;
    bool may_merge;
    FBound root_bounds;
    SInfo* stage;
    StageTilingInfo(){}
    StageTilingInfo(SInfo* s,FBound root_bounds,const FScope& settings):stage(s),root_bounds(root_bounds),may_merge(true){

        int pure_dims = 0;
        for(int d =0;d<s->loop.size();d++){
            auto const& l = s->loop[d];
            if(l.pure){
                pure_dims++;
            }
        }
        if(pure_dims != s->node->dimensions){
            may_merge = false;
        }


        for(int d =0;d<stage->loop.size();d++){

            auto const& l = stage->loop[d];
            int extent = root_bounds->loops(stage->index,d).extent();
            if(!l.pure){
                continue;
            }
            if(loops.size()>=3){
                break;
            }

            if(extent>=32){
                loops.push_back({Span(4,extent/8,true),l.pure_dim});
            }
        }


        //All the possible blocksizes
        std::vector<std::vector<std::vector<int>>> good_sizes = {
            {{256},{128},{64},{32},{16}},//sizes 1
            {{32,16},{32,8},{16,16},{16,8},{8,8},{4,4}}, //sizes 2
            {{8,8,8},{8,8,4},{{8,4,4}},{4,4,4},{4,4,2},{2,2,2}},//sizes 3
        };



        for(auto& i:good_sizes[loops.size()-1] ){
            bool fits= true;
            for(int d =0;d<loops.size();d++){
                if(loops[d].range.min()<=i[d] && i[d]<= loops[d].range.max()){
                    int p =1;
                    for(auto k:i){
                        p*=k;
                    }
                    if(p!=settings.threads){
                        fits=false;
                    }

                }
                else{
                    fits = false;
                }

            }
            if(fits){
                blocksizes.push_back(i);
            }

        }
        if(blocksizes.empty()){
            for(auto& i:good_sizes[loops.size()-1] ){
                bool fits= true;
                for(int d =0;d<loops.size();d++){
                    if(loops[d].range.min()<=i[d] && i[d]<= loops[d].range.max()){



                    }
                    else{
                        fits = false;
                    }

                }
                if(fits){
                    blocksizes.push_back(i);
                }

            }
        }

    }

};
void print(const std::vector<int>& t){
    std::cout<<"[";
    for(auto i :t){
        std::cout<<i<<",";
    }
    std::cout<<"]"<<std::endl;
}

//A group of function stages which will be scheduled in one kernel
struct KernelGroup{
    std::vector<SInfo*> stages;
    StageTiling tiling;
    FInfo* output_fn;
    SInfo* output_stage;
    bool is_small;
    KernelGroup(SInfo* stage,const std::vector<StageTilingInfo>& tile_info ,const FScope& settings,const dag::FunctionDAG& dag):stages({stage}),output_fn(stage->node),output_stage(stage){
        if(tile_info[stage->id].blocksizes.empty()){
            is_small = true;
        }
        else{
            tiling = find_best_tiling(settings,tile_info,0,dag);
            is_small = false;
        }

    }
    void dump(){
        std::cout<<"Kernel group "<< output_stage->name<<std::endl;
        if(is_small){
            std::cout<<"Small"<<std::endl;
            return;
        }
        std::cout<<"Block Tiling:"<<std::endl;
        print(tiling.outer_tiling);

        for(int i =0;i<stages.size();i++){
            std::cout<<"Stage: "<<stages[i]->name<<std::endl;
            std::cout<<"Reg Tiling:"<<std::endl;
            print(tiling.inner_tiling[i]);

        }
    };
    //Estimate the used shared memory a the sum of all regions computed
    int shared_memory(std::vector<dag::Bound>& bounds)const{

        int max_shared_memory =0;
        for(auto s:stages){
            if(s==output_stage){
                continue;
            }
            if(s->index==0){
                int shared_memory =1;
                for(int i =0;i<s->node->dimensions;i++){
                    shared_memory *= bounds[s->node->id]->region_computed(i).extent();
                }
                max_shared_memory+= shared_memory*s->node->bytes_per_point;

            }
        }

        /*
        while(shared.size()){
            std::vector<FInfo*> next_shared;
            int total_shared = 0;
            for(auto s:shared){
                bool endpoint = false;
                bool free=true;
                for(auto e:s->outgoing_edges){

                    for(auto si:shared){
                        endpoint |= (si!= e->consumer->node);
                        free &= (si!= e->consumer->node);

                    }

                }
                if(endpoint){
                    int shared_memory =1;
                    for(int i =0;i<s->dimensions;i++){
                        shared_memory *= bounds[s->id]->region_computed(i).extent();
                    }
                    shared_memory *=s->bytes_per_point;
                }
                total_shared+=total_shared+256;
                if(!free){
                    next_shared.push_back(s);
                }
            }
            shared=next_shared;
            if(total_shared>max_shared_memory){
                max_shared_memory=total_shared;
            }

        }
        */

        return max_shared_memory;
    }
    //Better Cost?
    double thread_div(int x,int b)const{
        return 1.0/exp( -(((x-1)%b)-b/2)/double(b)*5.0);
    }
    //Find the best tiling for this group according to the thesis 
    StageTiling find_best_tiling(const FScope& settings,const std::vector<StageTilingInfo>& tile_info,int block_size_id,const dag::FunctionDAG& dag)const{
        const auto& block_size = tile_info[output_stage->id].blocksizes[block_size_id];
        const auto& tile_dims=  tile_info[output_stage->id].loops;
        std::vector<int> outer_tiling(block_size.size());
        std::vector<int> inner_tiling(block_size.size());
        std::vector<std::pair<int,int>> outer_tile_range(block_size.size());
        std::vector<std::pair<int,int>> inner_tile_range(block_size.size());
        std::vector<int> required_tile_size(block_size.size());
        int threads= 1;
        for(int i = 0;i<block_size.size();i++){
            required_tile_size[i] = block_size[i]*settings.optimal_regsiter_tiling[i];
            outer_tile_range[i] = { block_size[i] - block_size[i] /2, block_size[i] + block_size[i] /2};
            outer_tiling[i] = outer_tile_range[i].first;
            inner_tiling[i] = 1;
            inner_tile_range[i] = {1,settings.max_regsiter_tiling[i]};
            threads *= block_size[i];
        }
        std::vector<dag::Bound> best_producer_bounds;
        double best_thread_usage = -1000;
        bool fits=true;
#define MODE 0
#if MODE
        while(fits){

            while(fits){

                std::vector<dag::Bound> tile_bounds(output_stage->node->max_id);
                dag::BoundContents* base =  tile_info[ output_stage->id].root_bounds->make_copy();
                for(int d = 0;d<block_size.size();d++){
                    base->loops(output_stage->index,tile_dims[d].pure_dim) = dag::Span(0,outer_tiling[d]*inner_tiling[d]-1,true);

                }

                tile_bounds[output_stage->node->id] = base;
                double thread_usage= 0;
                std::vector<bool> all_valid( block_size.size(),false);
                bool keeps_reg_bounds=true;

                for(const auto s:stages){
                    const auto& b = get_bounds(s->node,tile_bounds,dag);
                    for(int d = 0;d<block_size.size();d++){
                        int extent = b->loops(s->index,tile_dims[d].pure_dim).extent();
                        double val= (((extent-1)%(block_size[d])) )*s->arith_cost;
                        if(stages.size()==6 && outer_tiling[1]==8&&outer_tiling[0]==20&&inner_tiling[0]==3&&inner_tiling[1]==2){
                            std::cout<<val<<std::endl;
                        }
                        thread_usage += val;



                        all_valid[d] = all_valid[d]|(extent==required_tile_size[d]);
                        int unroll = (extent+block_size[d]-1)/block_size[d];
                        if(unroll==0){
                            keeps_reg_bounds=false;
                            break;
                        }
                        if(extent/unroll<block_size[d]/2){
                            keeps_reg_bounds=false;
                            break;
                        }
                        if(unroll>settings.max_regsiter_tiling[d]){
                            keeps_reg_bounds=false;
                            break;
                        }
                    }
                    if(!keeps_reg_bounds){
                        break;
                    }
                }
                if(keeps_reg_bounds){
                    bool valid=true;
                    for(auto b:all_valid){
                        valid &= b;
                    }
                    if(valid){
                        float occupancy = estimate_occupancy( threads,1,shared_memory(tile_bounds)).occupancy;
                        if(occupancy <0.5){
                            //fits=false;
                            break;
                        }
                        if(valid&&thread_usage>best_thread_usage){

                            best_producer_bounds = tile_bounds;
                            best_thread_usage = thread_usage;
                        }

                    }
                }



                bool end= false;
                for(int i = 0;i<block_size.size();i++){
                    if(inner_tiling[i] ==inner_tile_range[i].second ){
                        inner_tiling[i] = inner_tile_range[i].first;
                        if(i==block_size.size()-1){
                            end = true;
                        }
                        continue;
                    }
                    else{
                        inner_tiling[i] +=1;
                        break;
                    }

                }
                if(end){
                    break;
                }

            }
            bool end= false;
            for(int i = 0;i<block_size.size();i++){
                if(outer_tiling[i] ==outer_tile_range[i].second ){
                    outer_tiling[i] = outer_tile_range[i].first;
                    if(i==block_size.size()-1){
                        end = true;
                    }
                    continue;
                }
                else{
                    outer_tiling[i] +=1;
                    break;
                }

            }
            if(end){
                break;
            }
        }

        StageTiling tiling;
        if(best_producer_bounds.empty()){
            tiling.valid = false;
            return tiling;
        }
        //std::cout<<best_thread_usage<<std::endl;
        for(int d = 0;d<block_size.size();d++){
            tiling.threads *= block_size[d];
        }
        for(const auto s:stages){
            FBound b = best_producer_bounds[s->node->id];
            std::vector<int> reg_tile;

            for(int d = 0;d<block_size.size();d++){
                int extent = b->loops(s->index,tile_dims[d].pure_dim).extent();
                int unroll = (extent+block_size[d]-1)/block_size[d];
                reg_tile.push_back(unroll);
            }
            //std::cout<<"Scheduling "<<s->name<<" at [";
            for(int d = 0;d<block_size.size();d++){
                //std::cout<<tile_dims[d].pure_dim<<" "<<b->loops(s->index,tile_dims[d].pure_dim).extent()/reg_tile[d]<<",";

            }
           // std::cout<<"]\n";
            tiling.inner_tiling.push_back(reg_tile);
        };
        for(int i = 0;i<block_size.size();i++){
            FBound b = best_producer_bounds[output_stage->node->id];
            int extent = b->loops(output_stage->index,tile_dims[i].pure_dim).extent()/tiling.inner_tiling[0][i];
            //std::cout<<extent<<std::endl;
            tiling.outer_tiling.push_back(extent);
        }
        return  tiling;
#endif
#if not  MODE
        std::vector<int> best_outer_tile;
        while(fits){//Iterate all outer blocks
                while(fits){//Iterate all inner blocks

                    std::vector<dag::Bound> tile_bounds(output_stage->node->max_id);
                    dag::BoundContents* base =  tile_info[ output_stage->id].root_bounds->make_copy();
                    for(int d = 0;d<block_size.size();d++){
                        base->loops(output_stage->index,tile_dims[d].pure_dim) = dag::Span(0,outer_tiling[d]*inner_tiling[d]-1,true);

                    }

                    tile_bounds[output_stage->node->id] = base;
                    double thread_usage= 0;
                    std::vector<bool> all_valid( block_size.size(),false);
                    bool keeps_reg_bounds=true;
                    auto register_tile= [&](int extent,int d,SInfo* stage){
                          if(stage==output_stage){
                              return inner_tiling[d];
                          }
                          else{
                               int unroll = (extent+block_size[d]-1)/block_size[d];
                               return unroll;
                          }
                    };
                    for(const auto s:stages){
                        const auto& b = get_bounds(s->node,tile_bounds,dag);
                        for(int d = 0;d<block_size.size();d++){
                            int extent = b->loops(s->index,tile_dims[d].pure_dim).extent();
                            int unroll = register_tile(extent,d,s);
                            double val = ( (extent-1%block_size[d])*(register_tile(extent,d,s)==settings.optimal_regsiter_tiling[d]?2:1) )*s->arith_cost; //Cost
                            thread_usage +=val;

                            all_valid[d] = all_valid[d]|(extent==required_tile_size[d]);
                            if(unroll==0){
                                keeps_reg_bounds=false;
                                break;
                            }

                            if(extent/unroll<block_size[d]/2){
                                keeps_reg_bounds=false;
                                break;
                            }
                            if(unroll>settings.max_regsiter_tiling[d]){
                                keeps_reg_bounds=false;
                                break;
                            }
                        }
                        if(!keeps_reg_bounds){
                            break;
                        }
                    }
                    if(keeps_reg_bounds){
                        bool valid=true;
                        for(auto b:all_valid){
                            valid &= b;
                        }
                        if(valid){
                            float occupancy = estimate_occupancy( threads,1,shared_memory(tile_bounds)).occupancy;
                            if(occupancy <0.90){
                                //fits=false;
                                break;
                            }
                            if(valid&&thread_usage>best_thread_usage){
                                best_outer_tile=outer_tiling;
                                best_producer_bounds = tile_bounds;
                                best_thread_usage = thread_usage;
                            }

                        }
                    }



                    bool end= false;
                    for(int i = 0;i<block_size.size();i++){
                        if(inner_tiling[i] ==inner_tile_range[i].second ){
                            inner_tiling[i] = inner_tile_range[i].first;
                            if(i==block_size.size()-1){
                                end = true;
                            }
                            continue;
                        }
                        else{
                            inner_tiling[i] +=1;
                            break;
                        }

                    }
                    if(end){
                        break;
                    }

                }
                bool end= false;
                for(int i = 0;i<block_size.size();i++){
                    if(outer_tiling[i] ==outer_tile_range[i].second ){
                        outer_tiling[i] = outer_tile_range[i].first;
                        if(i==block_size.size()-1){
                            end = true;
                        }
                        continue;
                    }
                    else{
                        outer_tiling[i] +=1;
                        break;
                    }

                }
                if(end){
                    break;
                }
            }

            StageTiling tiling;
            tiling.outer_tiling=best_outer_tile;
            if(best_producer_bounds.empty()){
                tiling.valid = false;
                return tiling;
            }
            //std::cout<<best_thread_usage<<std::endl;
            for(int d = 0;d<block_size.size();d++){
                tiling.threads *= block_size[d];
            }
            //Set registertiling
            for(const auto s:stages){
                FBound b = best_producer_bounds[s->node->id];
                std::vector<int> reg_tile;

                for(int d = 0;d<block_size.size();d++){
                    int extent = b->loops(s->index,tile_dims[d].pure_dim).extent();
                    int unroll = (extent+block_size[d]-1)/block_size[d];
                    reg_tile.push_back(unroll);
                }
                //std::cout<<"Scheduling "<<s->name<<" at [";
                for(int d = 0;d<block_size.size();d++){
                    //std::cout<<tile_dims[d].pure_dim<<" "<<b->loops(s->index,tile_dims[d].pure_dim).extent()/reg_tile[d]<<",";

                }
               // std::cout<<"]\n";
                tiling.inner_tiling.push_back(reg_tile);
            };


        return tiling;
#endif
    }
    //Apply the tiling
    void schedule(std::vector<StageTilingInfo>& tile_info){
        if(is_small){
            return;
        }
        const auto& output = tile_info[output_stage->id];
        int stage_tile_dims = output.loops.size();
        Func final(stages[0]->node->func);
        if(!output.may_merge){
           return;
        }
        for(int i = 0;i<stages.size();i++){
            auto m =stages[i];
            if(i == 0){
                final.compute_root();
            }
            else if(m->index==0){
                Func f(m->node->func);
                //std::cout<<"compute "<<f.name()<<" at "<<final.name()<<std::endl;
                f.compute_at(final,final.args()[output.loops[0].pure_dim]);
            }
            std::vector<VarOrRVar> inner_tile_vars;
            for(int j =0;j<stage_tile_dims;j++){
                if(tiling.inner_tiling[i][j]<=1){
                    continue;
                }
                int pure_dim = output.loops[j].pure_dim;
                Var inner;
                m->stage.split(final.args()[pure_dim],final.args()[pure_dim],inner,tiling.inner_tiling[i][j]);
                inner_tile_vars.push_back(inner);
            }
            std::vector<VarOrRVar> outer_tile_vars;

            if(i == 0){
                for(int j =0;j<stage_tile_dims;j++){
                    if(tiling.inner_tiling[i][j]<1){
                        continue;
                    }
                    int pure_dim = output.loops[j].pure_dim;
                    Var inner;
                    m->stage.split(final.args()[pure_dim],final.args()[pure_dim],inner,tiling.outer_tiling[j]);
                    outer_tile_vars.push_back(inner);
                }
            }
            std::vector<VarOrRVar> no_tile;
            std::vector<VarOrRVar> tile;
            for(auto& l:m->loop){

                bool found= false;
                for(int j =0;j<stage_tile_dims;j++){
                    if(output.loops[j].pure_dim == l.pure_dim){
                        found = true;
                    }
                }
                if(!found){
                    no_tile.push_back(VarOrRVar(l.var,l.rvar));
                }
                else{
                    tile.push_back(VarOrRVar(l.var,l.rvar));
                }
            }
            std::vector<VarOrRVar> order= no_tile;
            order.insert(order.end(),inner_tile_vars.begin(),inner_tile_vars.end());
            order.insert(order.end(),outer_tile_vars.begin(),outer_tile_vars.end());
            order.insert(order.end(),tile.begin(),tile.end());
            m->stage.reorder(order);
            for(auto v:inner_tile_vars){
               m->stage.unroll(v);
            }
            if(i == 0){
              if(outer_tile_vars.size()==1){
                   m->stage.gpu_threads(outer_tile_vars[0]);
              }
              if(outer_tile_vars.size()==2){
                   m->stage.gpu_threads(outer_tile_vars[0],outer_tile_vars[1]);
              }
              if(outer_tile_vars.size()==3){
                   m->stage.gpu_threads(outer_tile_vars[0],outer_tile_vars[1],outer_tile_vars[2]);
              }
              if(stage_tile_dims==1 ){
                  m->stage.gpu_blocks(tile[0]);
              }
              if(stage_tile_dims==2 ){
                  m->stage.gpu_blocks(tile[0],tile[1]);
              }
              if(stage_tile_dims==3 ){
                  m->stage.gpu_blocks(tile[0],tile[1],tile[2]);
              }
            }
            else{
                if(stage_tile_dims==1 ){
                    m->stage.gpu_threads(tile[0]);
                }
                if(stage_tile_dims==2 ){
                    m->stage.gpu_threads(tile[0],tile[1]);
                }
                if(stage_tile_dims==3 ){
                    m->stage.gpu_threads(tile[0],tile[1],tile[2]);
                }
            }
        }
    }

};
bool operator !=(const Span&lhs,const Span& rhs){
    return lhs.min() == rhs.min() && lhs.min()== rhs.max();
}
//Add a function to a group
std::optional<KernelGroup> add_function(dag::FunctionDAG& dag,const KernelGroup& group,const std::vector<StageTilingInfo>& tile_info,FInfo* prod,const FScope& settings){
    if(group.is_small){
        return std::nullopt;
    }
    for(auto & s : prod->stages){
        if(!tile_info[s.id].may_merge){
            return std::nullopt;
        }
    }
    for (auto s:prod->outgoing_edges){
        if(std::find(group.stages.begin(),group.stages.end(),s->consumer) == group.stages.end()){
            return std::nullopt;
        }
    }

    if(tile_info[group.output_stage->id].loops.size() != tile_info[prod->stages.back().id].loops.size()){
        return std::nullopt;
    }
    for(int i =0;i<tile_info[group.output_stage->id].loops.size();i++){
        if(tile_info[group.output_stage->id].loops[i].range != tile_info[prod->stages.back().id].loops[i].range){
            return std::nullopt;
        }
    }

    KernelGroup new_group = group;
    for(auto & s : prod->stages){
        new_group.stages.push_back(&s);
    }
    for (int i =0;i<tile_info[new_group.output_stage->id].blocksizes.size();i++){
        StageTiling t = new_group.find_best_tiling(settings,tile_info,i,dag);
        if(t.valid){
           new_group.tiling = t;
           return new_group;
        }
        else{
            //std::cout<<"Invalid Tiling"<<std::endl;
        }
    }
    return std::nullopt;
}
//Schedule a Sector
void fuse_kernel( dag::FunctionDAG& dag,std::vector<std::string> sector,std::vector<FBound>& root_bounds,const FScope& settings){

    std::map<std::string,FInfo*> named_fn;
    for(auto& n:dag.nodes){
        named_fn[n.func.name()] = &n;
    }
    std::vector<std::shared_ptr< KernelGroup>> groups;

    std::vector<std::shared_ptr< KernelGroup>> group_mapping(dag.nodes[0].stages[0].max_id);
    std::vector<StageTilingInfo> tile_info(dag.nodes[0].stages[0].max_id);
    for(const auto& m:sector){
        for(auto& s: named_fn[m]->stages){
            tile_info[s.id] = StageTilingInfo(&s,root_bounds[s.node->id],settings);
        }
    }

    for(auto& s:named_fn[sector.back()]->stages){
        groups.push_back(std::shared_ptr<KernelGroup>( new KernelGroup(&s,tile_info,settings,dag)));
        group_mapping[s.id] = groups.back();
    }
    for(int i =sector.size()-2;i>=0;i--){
        const auto& m = sector[i];
        std::shared_ptr< KernelGroup> common_group = group_mapping[named_fn[m]->outgoing_edges[0]->consumer->id];
        for(auto e: named_fn[m]->outgoing_edges){
            if(group_mapping[e->consumer->id] != common_group){
                common_group = {};
            }
        }
        bool merged =  false;
        if(common_group){
            auto new_group = add_function(dag,*common_group,tile_info,named_fn[m],settings);
            if(new_group.has_value()){
                *common_group=new_group.value();
                //std::cout<<"Merged "<<m<<" into"<<common_group->output_stage->name<<std::endl;
                //common_group->dump();
                merged = true;
                for(auto& s:named_fn[m]->stages){
                    group_mapping[s.id] = common_group;
                }
            }
        }
        if(!merged){
            for(auto& s:named_fn[m]->stages){
                groups.push_back(std::shared_ptr<KernelGroup>( new KernelGroup(&s,tile_info,settings,dag)));
                group_mapping[s.id] = groups.back();
            }
        }
    }
    for(auto g:groups){
        //g->dump();
        g->schedule(tile_info);
    }
}








void SemiScheduler::add_auto(Func& function,std::string group){
    autoschedule[function.name()] = {group};
}
void SemiScheduler::add_custom(Func& function,std::function<void()> callback){
    customschedule[function.name()] = callback;
}
void SemiScheduler::schedule(Pipeline& pipeline){

    std::vector<Function> outputs={};
    std::vector<std::string> output_names;
    std::map<std::string,Function> env;
    std::map<std::string,std::vector<std::string>> called_by;
    for(auto& i:pipeline.outputs()){
        outputs.push_back(i.function());
        output_names.push_back(i.name());
    }
    for (const auto &func : outputs) {
        std::map<std::string, Function> local_env = find_transitive_calls(func);
        env.insert(local_env.begin(), local_env.end());
    }
    for (auto &iter : env) {
        iter.second.lock_loop_levels();
    }
    std::vector<std::string> order = topological_order(outputs, env);
    std::vector<std::string> affine_order;
    for (int i = 0; i < (int)order.size();i++) {
        if(autoschedule.find(order[i]) != autoschedule.end()){
            affine_order.push_back(order[i]);
        }
    }

    //std::cout<<"Inlining none affine"<<std::endl;
    {
        bool inlined_one = false;
        do{
            inlined_one=false;
            for(int i = affine_order.size()-1;i>=0;i--){
                for(const auto& [name,fn]: find_direct_calls(env[affine_order[i]])){
                    if(autoschedule.find(name) == autoschedule.end()&&customschedule.find(name) == customschedule.end() ){
                        inline_function(env[affine_order[i]],env[name]);
                        //std::cout<<"inling  function "<<name<<" in "<< affine_order[i]<<std::endl;
                        inlined_one = true;
                    }
                }
            }
            env = {};
            for (const auto &func : outputs) {
                std::map<std::string, Function> local_env = find_transitive_calls(func);
                env.insert(local_env.begin(), local_env.end());
            }
            order = topological_order(outputs, env);
            affine_order ={};
            for (int i = 0; i < (int)order.size();i++) {
                if(autoschedule.find(order[i]) != autoschedule.end()){
                    affine_order.push_back(order[i]);
                }
            }

        }while(inlined_one);
    }
    //std::cout<<"Inlining elmentwise affine Function"<<std::endl;
    {
        bool inlined = false;
        do{
            inlined = false;
            // The very last few functions in 'order' are the last to be realized in the
            // pipeline (the final producers) so there is no point in checking it.
            for (int i = 0; i < (int)order.size() - (int)outputs.size(); ++i) {
                bool is_output = false;
                for (const Function &f : outputs) {
                    if (order[i] == f.name()) {
                        is_output = true;
                        break;
                    }
                }
                if (is_output||  autoschedule.find(order[i]) == autoschedule.end()) {
                    // Should not inline output Func
                    //std::stringstream() << "Skip inlining " << order[i] << " since it is not affine\n";
                    continue;
                }
                std::string caller = is_func_called_element_wise(order, i, env);
                if (!caller.empty()) {
                    //std::cout<<"inling elementwise affine function "<<order[i]<<" in "<< caller <<std::endl;
                    inlined = true;
                    inline_function(env.at(caller), get_element(env, order[i]));
                }
            }
            if(inlined){
                env = {};
                for (const auto &func : outputs) {
                    std::map<std::string, Function> local_env = find_transitive_calls(func);
                    env.insert(local_env.begin(), local_env.end());
                }
                order = topological_order(outputs, env);

            }
        }while(inlined);
    }
    std::vector<std::string> real_order = realization_order(outputs, env).first;
    FuncValueBounds func_val_bounds = compute_function_value_bounds(real_order, env);
    RegionCosts costs(env,real_order);
    dag::FunctionDAG dag(env,order,outputs);
    for (int i = 0; i < order.size(); i++) {
        for(auto [name,fn]:find_direct_calls(env[order[i]])){
            if(name != order[i]){
                called_by[name].push_back(order[i]);
            }
            else{
               // std::cout<<name<<std::endl;
            }

        }
        if(autoschedule.find(order[i]) != autoschedule.end()){

            affine_order.push_back(order[i]);
        }
    }
    //std::cout<<"Partioning Filters"<<std::endl;
    std::map<std::string,std::string> parition;
    for(int i = order.size()-1;i>=0;i--){

        if(autoschedule.find(order[i]) == autoschedule.end()){
            parition[order[i]] = "$custom_schedule";
        }
        else if(std::find(output_names.begin(),output_names.end(),order[i]) != output_names.end()){
            parition[order[i]] = order[i];
        }else{
            bool pure=true;
            auto& fn = autoschedule.at(order[i]);
            std::string l = parition[called_by[order[i]][0]];
            for(auto&name:called_by[order[i]]){
                if(parition[name] !=l||autoschedule.find(name) == autoschedule.end() || autoschedule.at(name).scope !=autoschedule.at(order[i]).scope){
                    pure = false;
                }
            }
            if(pure){
                parition[order[i]] = l;
            }
            else{
                parition[order[i]] = order[i];
            }
        }
    }
    std::map<std::string, std::vector<std::string>> partitions;
    for(int i = 0;i<order.size();i++){
        partitions[parition[order[i]]].push_back(order[i]);
    }
    partitions.erase("$custom_schedule");
    std::vector<FBound> estimates(dag.nodes[0].max_id);
    for(auto& e:dag.nodes){
        get_bounds(&e,estimates,dag);
    }
    for(auto& [base,members]:partitions){
        fuse_kernel(dag,members,estimates,scopes[ autoschedule[base].scope]);
    }
    for(auto& [name,fn]:customschedule){
        fn();
    }
}

