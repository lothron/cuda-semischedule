#!/bin/bash
mkdir -p build
cd build 
cmake ..
make
cp -a ../test-images/. .
export HL_CUDA_JIT_MAX_REGISTERS=256
export HL_DEBUG_CODEGEN
export HL_GPU_DEVICE=0
export HL_GPU_GLOBAL_COST=1
export HL_GPU_L2_COST=200
export HL_GPU_SHARED_COST=1
export HL_PERMIT_FAILED_UNROLL=1
export HL_TARGET=host-cuda-cuda_capability_61
echo testing laplace
./app laplace
echo testing harris
./app harris
echo testing pipeline
./app pipeline











