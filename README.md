# Halide Cuda
This project contains the source code and performance tests of my bachelor thesis about halide scheduling on cuda devices.  
## Getting Started
### Build Requirements
1. halide
2. gcc
3. cmake
4. make

Simply execute the run_tests.sh. To build the benchmarks cmake needs to find halide in PATH.  
The relevant code can be found the *semiautoschedule.h* and *semiautoschedule.cpp* files.  









